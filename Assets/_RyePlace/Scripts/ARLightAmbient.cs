﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

//This script can only be attached to a gameobject that has a Light component
[RequireComponent(typeof(Light))]
public class ARLightAmbient : MonoBehaviour
{
    //Light Variable
    private Light light;

    void Start()
    {
        //Get light component attached to gameobject this script is attached to
        light = GetComponent<Light>();

        //When a camera frame is received, OnCameraFrameReceived() will also run
        ARSubsystemManager.cameraFrameReceived += OnCameraFrameReceived;
    }

    //Function runs/is updated everytime the ARSusbsystemManager receives a new camera frame
    void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
    {
        //Update light intensity and colorTemperature based on the camera frame
        light.intensity = eventArgs.lightEstimation.averageBrightness.Value;
        light.colorTemperature = eventArgs.lightEstimation.averageColorTemperature.Value;
    }

    //Function disabled OnCameraFrameReceived() when disabled
    void OnDisable()
    {
        
        ARSubsystemManager.cameraFrameReceived -= OnCameraFrameReceived;
    }
}
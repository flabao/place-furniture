﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class InteractManager : MonoBehaviour {

    Spawner spawner;

    //Object variables
    public GameObject bed;
    public GameObject chair;
    public GameObject sofa;
    public GameObject table;
    public GameObject cabinet;
    public GameObject rack;

    private bool activeSelf;
    //UI variables
    public GameObject objectSelectionPanel;

    private void Awake()
    {
        if (spawner == null)
            spawner = GetComponent<Spawner>();
    }

    //Used for button. Whenever objectSelectionButton is pressed, it will enable/disable
    public void ToggleObjectSelection()
    {
        //Implement. NOTE, use objectSelectionPanel, setActive() and activeSelf
        // ! - represents NOT bool, performing an opposite effect.
        activeSelf = !activeSelf;
        objectSelectionPanel.SetActive(activeSelf);
    }

    //Set spawner object to bed, toggle object selection
    public void SpawnBed()
    {
        spawner.SetSpawnObject(bed);
        ToggleObjectSelection();
    }

    //Set spawner object to chair, toggle object selection
    public void SpawnChair()
    {
        spawner.SetSpawnObject(chair);
        ToggleObjectSelection();
    }

    //Set spawner object to sofa, toggle object selection
    public void SpawnSofa()
    {
        spawner.SetSpawnObject(sofa);
        ToggleObjectSelection();
    }

    //Set spawner object to table, toggle object selection
    public void SpawnTable()
    {
        spawner.SetSpawnObject(table);
        ToggleObjectSelection();
    }

    //Set spawner object to cabinet, toggle object selection
    public void SpawnCabinet()
    {
        spawner.SetSpawnObject(cabinet);
        ToggleObjectSelection();
    }

    //Set spawner object to rack, toggle object selection
    public void SpawnRack()
    {
        spawner.SetSpawnObject(rack);
        ToggleObjectSelection();
    }

    //Used for button. Reload level when this button is clicked.
    public void ResetLevel()
    {
        //using UnityEngine.sceneManagement package gives access to Scene management functionality
        //Use 'SceneManager' to reload level. HINT. LoadScene
        SceneManager.LoadScene("SampleScene");
    }


}
